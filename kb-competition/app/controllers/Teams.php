<?php
  class Teams extends Controller {
    public function __construct(){
        if(!isLoggedIn()){
            redirect('users/login');
        }
       $this->teamModel = $this->model('Team');
       $this->userModel = $this->model('User');
    }

    public function team(){
        
        $team = $this->teamModel->getTeams();
        $data = [
            'teams' => $team
        ];
        $this->view('competition/team/team', $data);

    }
    public function team2(){
        
    
        $team = $this->teamModel->getTeams();
        $data = [
            'teams' => $team
        ];
        $this->view('competition/player/insertingone', $data);

    }

    public function insertingone(){
    
        // dit zal checken of het om een POST actie gaat 
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
    // het formulier uitvoeren
    
    //Sanitize post data 
    $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
    
    $data =[
        'Name' => trim($_POST['Name']),
        'Location' => trim($_POST['Location']),
        'Score' => trim($_POST['Score']),
        'Name_err' => '',
        'Location_err' =>'',
        'Score_err' => ''
    ];
    
    // validate name
    if(empty($data['Name'])){
        $data['Name_err'] = 'Please enter a team name';
    }
    // validate location
    if(empty($data['Location'])){
        $data['Location_err'] = 'Please enter a location';
    }
    // validate score
    if(empty($data['Score'])){
        $data['Score_err'] = 'Please enter a score';
    }

    
    // zorg dat alle error's leeg zijn
    if(empty($data['Name_err']) && empty($data['Location_err']) && empty($data['Score_err']))
    {
        // validated
       // die('SUCCESS');
    
    if($this->teamModel->insertingone($data)){
        flash('register_success', 'The team has been added');
        redirect('teams/Team');
        
    } else{
        die('Something went wrong');
    }
    
    } else{
        // load view with errors
        $this->view('competition/team/insertingone', $data);
    
    }
    
     } else {
            // het formulier laden ( test hieronder of de url football/insertplayer werkt met de echo)
            //echo 'laadt de spelerslijst';
    //initialiseer data 
    $data =[
        'Name' =>  '',
        'Location' =>  '',
        'Score' =>  '',
        'Name_err' => '',
        'Location_err' =>'',
        'Score_err' => ''

    ];
    
    // load view
    $this->view('competition/team/insertingone', $data);
    
        }
    }

    public function showTeam($id){

        $team = $this->teamModel->getTeamById($id);
        
        $data = [
            'team' => $team,
          
        ];
    
    $this->view('competition/team/showteam', $data);
    
    }

    public function editTeam($id){
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            // SANITIZE POST array
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
            $data = [
    
                'Id' => $id,
                'Name' => trim($_POST['Name']),
                'Location' => trim($_POST['Location']),
                'Score' => trim($_POST['Score']),
                
                        ];
                        // validate data
                        // validate title
                        if(empty($data['Name'])){
                            $data['Name_err'] = 'Please enter a name';
                        }
                        if(empty($data['Location'])){
                            $data['Location_err'] = 'Please enter a location';
                        }
                        if(empty($data['Score'])){
                            $data['Score_err'] = 'Please enter a score';
                        }

                     
    
                        // make sure there are no errors  
                        if(empty($data['Name_err']) && empty($data['Location_err']) && empty($data['Score_err']) ){
                            // validated
                            if($this->teamModel->updateTeam($data)){
                                
                                // hier is nog iets niet juist...
                                flash('post_message', 'Team Updated');
                                redirect('team'); 
    
                            }else{
                                die('Something went wrong');
                            }
    
                        }else {
                            // load the view with errors
                            $this->view('competition/team/editteam', $data);
                        }
    
        } else{
    
            // get the existing team from the model
            $team = $this->teamModel->getTeamById($id);
    
    
    
            // Check if the logged in user is the owner of the post.. if it's not, then we'll redirect the user away from the edit form
            // if($post->user_id != $_SESSION['user_id']){
            //     redirect('posts');
            // }
    
    
            // hier gaan we de waardes meegeven aan onze view door de variable $data op te vullen
            $data = [
        'Name' =>  $team->Name,
        'Location' =>$team->Location,
        'Score' =>  $team->Score,
        'Id' => $team->Id,       
            ];
    
            // we nemen dus de id etc..  en geven die mee via $data aan de edit view
            // dit wordt dan samen aan de gebruiker getoond via de browser door de controler.
            $this->view('competition/team/editteam', $data);
        }
    }



}