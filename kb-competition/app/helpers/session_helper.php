<?php
session_start();

// Flash message helper
// EXAMPLE - flash('register_success' 'you are now registered');
// DISPLAY IN VIEW - <?ph.  echo flash ('register_success'); ?.>
function flash($flash = '', $message = '', $class = 'alert alert-success') {
 
    if (!empty($flash) and !empty($message) and empty($_SESSION[$flash])) {
 
        $_SESSION[$flash] = $message;
        $_SESSION[$flash . '_class'] = $class;
 
    } elseif (empty($message) and !empty($_SESSION[$flash])) {
 
        $class = !empty($_SESSION[$flash . '_class']) ? $_SESSION[$flash . '_class'] : '';
        echo '<div class="' . $class . '" id="msg-flash">' . $_SESSION[$flash] . '</div>';
        unset($_SESSION[$flash]);
        unset($_SESSION[$flash . '_class']);
    }
 
}

 function isLoggedIn(){
    if(isset($_SESSION['user_id'])){
        return true;
    }else{
        return false;
    }
}


// function flash($name = '', $message = '', $class = 'alert alert-success'){
// if(!empty($name)){
//     if(!empty($messsage) && empty($_SESSION[$name])){
//         if(!empty($_SESSION[$name])){
//             unset($_SESSION[$name]);
//         }

//         if(!empty($_SESSION[$name. '_class'])){
//             unset($_SESSION[$name. '_class']);
//         }



//         $_SESSION[$name] = $message;
//         $_SESSION[$name. '_class'] = $class;
//     } elseif(empty($messsage) && !empty($_SESSION[$name])){
// $class = !empty($_SESSION[$name. '_class']) ? $_SESSION[$name. '_class'] : '';
// echo '<div class="'.$class.'" id=msg-flash">'.$_SESSION[name].'</div>';
// unset($_SESSION[$name]);
// unset($_SESSION[$name. '_class']);
//     }
// }
// }