<?php
/* App core class
creates url and loads core controller
ur - format - controller/method/params
*/

/// deze klasse wordt geladen als de index.php pagina opgeladen wordt. ( $init = new Core; )
// op dat moment wordt een nieuwe instantie gemaakt van deze klasse en als default controller staat er momenteel Pages en roept de default method index op en zet een lege aray
class Core{
    protected $currentController = 'Pages';
    protected $currentMethod = 'index';
    protected $params = [];

    // dit is de constructor, deze heeft als eerste taak, de url uitlezen via de getUrl functie die onderaan deze pagina beschreven staat. 
    // als de url eruitgehaald is wordt de waarde hiervan in de variable $url gezet. 
    public function __construct(){
        //print_r($this->getUrl());
        $url = $this->getUrl();

        // Als de url die uitgelezen wordt waardes heeft die bestaan dan wordt hiermee iets gedaan. 
        // hieronder wordt de eerste waarde of beter gezegd, het eerste deel van de url uitgelezen (vb. posts/edit/1)in dit geval behandelen we het stukje edit
        if(file_exists('../app/controllers/' . ucwords($url[0]). '.php')){
            // if exists, set as controller
            $this->currentController = ucwords($url[0]);

            // unset 0 index
            unset($url[0]);
        }

        // require the controller
        require_once '../app/controllers/'. $this->currentController . '.php';


        // instantiate controller class
        $this->currentController = new $this->currentController;


        // check for second value ( second part of url)
        if(isset($url[1])){
            // check if method exists in controller

            if(method_exists($this->currentController, $url[1])){
                $this->currentMethod = $url[1];

                // Unset 1 index
                unset($url[1]);
            }
        }

        //Get params
        $this->params = $url ? array_values($url) : [];

        // Call a callback with array of params
        call_user_func_array([$this->currentController, $this->currentMethod], $this->params);
     }
        
    public function getUrl(){
    if(isset($_GET['url'])){
        $url = rtrim($_GET['url'], '/');
        $url = filter_var($url, FILTER_SANITIZE_URL);
        $url = explode('/', $url);
        return $url;
     }
    }
}

?>



