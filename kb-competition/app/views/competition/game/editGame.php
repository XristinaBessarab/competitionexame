<header >
  <?php require APPROOT . '/views/inc/header.php'; ?>
  <?php require APPROOT . '/views/inc/navbar.php' ?>
  
</header>
<main>
    
  <article>
    
   <a href="<?php echo URLROOT; ?>/games/game" class="btn btn-light">cancel</a>
      <div class="card card-body bg-light mt-5">
   
        <h2>Edit Game</h2>
        <p>Use this form to edit a game</p>
        <form action="<?php echo URLROOT; ?>/games/editgame/<?php echo $data['Id']; ?>" method="post">
          
          <div class="form-group">
            <label for="Date">Date: <sup>*</sup></label>
            <input type="text" name="Date" class="form-control form-control-lg <?php echo (!empty($data['Date_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['Date']; ?>">
            <span class="invalid-feedback"><?php echo $data['Date_err']; ?></span>
          </div>
          <div class="form-group">
            <label for="Location">Status: <sup>*</sup></label>
            <input type="text" name="Status" class="form-control form-control-lg <?php echo (!empty($data['Status_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['Status']; ?>">
            <span class="invalid-feedback"><?php echo $data['Status_err']; ?></span>
          </div>
<div class="form-group">
            <label for="ScoreHome">ScoreHome: <sup>*</sup></label>
            <input type="text" name="ScoreHome" class="form-control form-control-lg <?php echo (!empty($data['ScoreHome_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['ScoreHome']; ?>">
            <span class="invalid-feedback"><?php echo $data['ScoreHome_err']; ?></span>
          </div>
          <div class="form-group">
            <label for="ScoreVisitors">ScoreVisitors: <sup>*</sup></label>
            <input type="text" name="ScoreVisitors" class="form-control form-control-lg <?php echo (!empty($data['ScoreVisitors_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['ScoreVisitors']; ?>">
            <span class="invalid-feedback"><?php echo $data['ScoreVisitors_err']; ?></span>
          </div>
          <!-- <div class="form-group">
            <label for="body">Body: <sup>*</sup></label>
            <textarea name="body" name="password" class="form-control form-control-lg < ?php echo (!empty($data
            //['body_err'])) ? 'is-invalid' : ''; ?>">< ?php echo $data //[ 'body'];?></textarea>
            <span class="invalid-feedback">< ?php echo $data //['body_err']; ?></span>
          </div> -->
          <div class="dropdown">
            <label for="TeamHomeId"> Home Team </label>
                        <select id="TeamHomeId" name="TeamHomeId" class="form-control form-control-lg " style="width: 100%">
                             <?php
                                if ($data) {
                                    foreach ($data['teams'] as $team) {
                                        $selected = ($team->Id == $data['TeamHomeId']) ? 'selected' : '';
                            ?>
                                    <option value="<?php echo $team->Id . ' / '. $data['TeamHomeId'];?>" <?php echo $selected?>>
                                        <?php echo $team->Name; ?>
                                    </option>
                            <?php
                                    }
                                }
                            ?>
                            </select>
          </div>


          <div class="dropdown">
            <label for="TeamVisitorId">Visitor team: </label>
                        <select id="TeamVisitorId" name="TeamVisitorId" class="form-control form-control-lg " style="width: 100%">
                             <?php
                                if ($data) {
                                    foreach ($data['teams'] as $team) {
                                        $selected = ($team->Id == $data['TeamVisitorId']) ? 'selected' : '';
                            ?>
                                    <option value="<?php echo $team->Id . ' / '. $data['TeamVisitorId'];?>" <?php echo $selected?>>
                                        <?php echo $team->Name; ?>
                                    </option>
                            <?php
                                    }
                                }
                            ?>
                            </select>
          </div>


          <div class="dropdown">
            <label for="LigaId">Liga: </label>
                        <select id="LigaId" name="LigaId" class="form-control form-control-lg " style="width: 100%">
                             <?php
                                if ($data) {
                                    foreach ($data['league'] as $league) {
                                        $selected = ($league->Id == $data['LigaId']) ? 'selected' : '';
                            ?>
                                    <option value="<?php echo $league->Id . ' / '. $data['LigaId'];?>" <?php echo $selected?>>
                                        <?php echo $league->Name; ?>
                                    </option>
                            <?php
                                    }
                                }
                            ?>
                            </select>
          </div>

        <input type='submit' class="btn btn-success" value="Submit">
        </form>
    </div>
