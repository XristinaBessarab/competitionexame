<header >
  <?php require APPROOT . '/views/inc/header.php'; ?>
  <?php require APPROOT . '/views/inc/navbar.php' ?>
  
</header>
<main>
    
  <article>

<?php require APPROOT . '/views/competition/compnavbar.php' ?>

<nav class="navbar navbar-expand-lg navbar-dark">

        <ul class="navbar-nav mr-auto">
          <li class="nav-item">   
            <a class="nav-link" href="<?php echo URLROOT; ?>/games/insertingone">Add a game</a>
          </li>

        </ul>
      
    </div>
  </nav>
  
  </article>

  <nav>side nav</nav>

  <aside>
  
  <?php foreach($data['games'] as $game) : ?>  

<!-- <div class=sidelist> -->
<div class="card card-body mb-4">game
<h4 class="card-title">  <?php echo $game->Date;?></h4>
<p class="card-text"><?php echo $game->Status; ?></p>
<a href="<?php echo URLROOT; ?>/games/showgame/<?php echo $game->Id; ?>" class="btn btn-dark">More</a>

</div>
<?php endforeach; ?>
  
  </aside>

</main>

<footer>
  footer
</footer>

</body>
  
<?php require APPROOT . '/views/inc/footer.php'; ?>