<header >
  <?php require APPROOT . '/views/inc/header.php'; ?>
  <?php require APPROOT . '/views/inc/navbar.php' ?>
 
</header>
<main>
    
  <article>
      
  <?php require APPROOT . '/views/competition/compnavbar.php' ?>

        <h2 >Add a game</h2>
        <p id=donald>Please fill out this form to add a game</p>
        
        <!-- <form action="< ?php echo URLROOT; ?>/competition/team/insertingone" method="post">    deze fout had ik gemaakt waar ik uren op gezocht heb. ik had naar de view directory
        verwezen ipv naar de controler. hierdoor kreeg ik geen enkele error pagina te zien en werd er ook niks in de db geschreven-->

        <form action="<?php echo URLROOT; ?>/games/insertingone" method="post">
          <div class="form-group">
            <label for="Name">Date: <sup>*</sup></label>
            <input type="date" name="Date" class="form-control form-control-lg <?php echo (!empty($data['Date_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['Date']; ?>">
            <span class="invalid-feedback"><?php echo $data['Date_err']; ?></span>
          </div>
          <div class="form-group">
            <label for="Location">Status: <sup>*</sup></label>
            <input type="text" name="Status" class="form-control form-control-lg <?php echo (!empty($data['Status_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['Status']; ?>">
            <span class="invalid-feedback"><?php echo $data['Status_err']; ?></span>
          </div>
          <div class="form-group">
            <label for="ScoreHome">Score Home: <sup>*</sup></label>
            <input type="ScoreHome" name="ScoreHome" class="form-control form-control-lg <?php echo (!empty($data['ScoreHome_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['ScoreHome']; ?>">
            <span class="invalid-feedback"><?php echo $data['ScoreHome_err']; ?></span>
          </div>
          <div class="form-group">
            <label for="ScoreVisitors">Score Visitors: <sup>*</sup></label>
            <input type="ScoreVisitors" name="ScoreVisitors" class="form-control form-control-lg <?php echo (!empty($data['ScoreVisitors_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['ScoreVisitors']; ?>">
            <span class="invalid-feedback"><?php echo $data['ScoreVisitors_err']; ?></span>
          </div>
          
         <div class="dropdown">
            <label for="TeamHomeId">Home Team <sup>*</sup></label>
                        <select id="TeamHomeId" name="TeamHomeId" class="form-control form-control-lg " style="width: 100%">
                             <?php
                                if ($data) {
                                    foreach ($data['teams'] as $team) {
                            ?>
                                    <option value="<?php echo $team->Id;?>">
                                        <?php echo $team->Name; ?>
                                    </option>
                            <?php
                                    }
                                }
                            ?>
                            </select>
          </div>

          <div class="dropdown">
            <label for="TeamVisitorId">Visitors Team <sup>*</sup></label>
                        <select id="TeamVisitorId" name="TeamVisitorId" class="form-control form-control-lg " style="width: 100%">
                             <?php
                                if ($data) {
                                    foreach ($data['teams'] as $team) {
                            ?>
                                    <option value="<?php echo $team->Id;?>">
                                        <?php echo $team->Name; ?>
                                    </option>
                            <?php
                                    }
                                }
                            ?>
                            </select>
          </div>

          <div class="dropdown">
            <label for="LigaId">Liga <sup>*</sup></label>
                        <select id="LigaId" name="LigaId" class="form-control form-control-lg " style="width: 100%">
                             <?php
                                if ($data) {
                                    foreach ($data['league'] as $league) {
                            ?>
                                    <option value="<?php echo $league->Id;?>">
                                        <?php echo $league->Name; ?>
                                    </option>
                            <?php
                                    }
                                }
                            ?>
                            </select>
          </div>
<br></br>

          <div class="row">
            <div class="col">
              <input type="submit" value="Add Game" class="btn btn-success btn-block">
            </div>
          
          </div>
        </form>

</div>
  
  </article>

  <nav>side nav</nav>

<aside>

</aside>

</main>
<footer>
footer
<?php require APPROOT . '/views/inc/footer.php'; ?>
</footer>

</body>
  
