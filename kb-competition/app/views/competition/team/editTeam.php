<header >
  <?php require APPROOT . '/views/inc/header.php'; ?>
  <?php require APPROOT . '/views/inc/navbar.php' ?>
  
  
</header>
<main>
    
  <article>
    


  
   <a href="<?php echo URLROOT; ?>/teams/team" class="btn btn-light">cancel</a>
      <div class="card card-body bg-light mt-5">
   
        <h2>Edit Team</h2>
        <p>Use this form to edit a team</p>
        <form action="<?php echo URLROOT; ?>/teams/editteam/<?php echo $data['Id']; ?>" method="post">
          
          <div class="form-group">
            <label for="Name">Name: <sup>*</sup></label>
            <input type="text" name="Name" class="form-control form-control-lg <?php echo (!empty($data['Name_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['Name']; ?>">
            <span class="invalid-feedback"><?php echo $data['Name_err']; ?></span>
          </div>
          <div class="form-group">
            <label for="Location">Location: <sup>*</sup></label>
            <input type="text" name="Location" class="form-control form-control-lg <?php echo (!empty($data['Location_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['Location']; ?>">
            <span class="invalid-feedback"><?php echo $data['Location_err']; ?></span>
          </div>
<div class="form-group">
            <label for="Score">Score: <sup>*</sup></label>
            <input type="text" name="Score" class="form-control form-control-lg <?php echo (!empty($data['Score_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['Score']; ?>">
            <span class="invalid-feedback"><?php echo $data['Score_err']; ?></span>
          </div>
          <!-- <div class="form-group">
            <label for="body">Body: <sup>*</sup></label>
            <textarea name="body" name="password" class="form-control form-control-lg < ?php echo (!empty($data
            //['body_err'])) ? 'is-invalid' : ''; ?>">< ?php echo $data //[ 'body'];?></textarea>
            <span class="invalid-feedback">< ?php echo $data //['body_err']; ?></span>
          </div> -->
        <input type='submit' class="btn btn-success" value="Submit">
        </form>
    </div>
